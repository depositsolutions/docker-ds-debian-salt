FROM debian:9

RUN apt-get update && apt-get upgrade -y && apt-get install -y apt-utils gnupg apt-transport-https curl && \
    curl -sSO https://repo.saltstack.com/apt/debian/9/amd64/2017.7/SALTSTACK-GPG-KEY.pub && \
    apt-key add SALTSTACK-GPG-KEY.pub && \
    echo "deb http://repo.saltstack.com/apt/debian/9/amd64/2017.7 stretch main" > /etc/apt/sources.list.d/salt-2017.7.7.list && \
    apt-get update && \
    apt-get install -y \
    sudo \
    udev \
    openssh-server \
    locales \
    lsb-release locales \
    net-tools \
    libpci3 \
    libpython-stdlib \
    libpython2.7-minimal \
    libpython2.7-stdlib \
    libyaml-0-2\ 
    python \
    python-minimal \
    python2.7 \
    python2.7-minimal \
    python-doc \
    python-tk \
    python2.7-doc \
    binutils \
    binfmt-support \
    iso-codes \
    javascript-common \
    libjs-jquery \
    libjs-sphinxdoc \
    libjs-underscore \
    libpgm-5.2-0 \
    libsodium18 \
    python-apt-common \
    python-cffi-backend \
    python-chardet \
    python-concurrent.futures \
    python-cryptography \
    python-enum34 \
    python-idna \
    python-ipaddress \
    python-markupsafe \
    python-openssl \
    python-pkg-resources \
    python-pyasn1 \
    python-setuptools \
    python-singledispatch \
    python-six \
    python-urllib3 \
    libzmq5 \
    python-apt \
    python-backports-abc \
    python-crypto \
    python-jinja2 \
    python-m2crypto \
    python-msgpack \
    python-requests \
    python-systemd \
    python-tornado \
    python-zmq \
    python-jinja2 \
    git-man \
    less \
    liberror-perl \
    libgdbm3 \
    libperl5.24 \
    libpopt0 \
    netbase \
    patch \
    perl \
    perl-modules-5.24 \
    rename \
    rsync \
    virt-what \
    salt-master \
    salt-minion && \
    locale-gen en_US.UTF-8
