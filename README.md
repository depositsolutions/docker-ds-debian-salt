# Custom Debian-Salt Build Container
[![pipeline status](https://gitlab.com/depositsolutions/docker-ds-debian-salt/badges/master/pipeline.svg)](https://gitlab.com/depositsolutions/docker-ds-debian-salt/commits/master)

This project contains the dockerfile for a custum debian base image of gitlab-ci jobs.

## Dependencies
  * Installed packages for test-kitchen
  * Saltstack Repository for version 2017.07
  * Systemd enabled


## HowTo
Build docker image to update the included packages
```
docker build . -t  registry.gitlab.com/depositsolutions/docker-ds-debian-salt:latest
```

Upload image to the registry
```
docker push registry.gitlab.com/depositsolutions/docker-ds-debian-salt:latest
```

Use image in gitlab-ci.yml
```
image: registry.gitlab.com/depositsolutions/docker-ds-debian-salt:latest
```
